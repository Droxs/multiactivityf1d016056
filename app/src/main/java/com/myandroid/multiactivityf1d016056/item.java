package com.myandroid.multiactivityf1d016056;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class item extends AppCompatActivity {
    TextView dataNama;
    TextView dataNim;
    TextView dataAlamat;
    int getPosisi;
    Button TomEdit;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);

        dataNama = findViewById(R.id.tesNama);
        dataNim = findViewById(R.id.tesNim);
        dataAlamat = findViewById(R.id.tesAlamat);
        TomEdit = findViewById(R.id.TomEdit);

        Intent intent = getIntent();
        getPosisi = intent.getIntExtra("posisi",0);
        String getDataNama = intent.getStringExtra("nama");
        String getDataNim = intent.getStringExtra("nim");
        String getDataAlamat = intent.getStringExtra("alamat");

        dataNama.setText("Nama : " + getDataNama);
        dataNim.setText("NIM   : " + getDataNim);
        dataAlamat.setText("Alamat :" + getDataAlamat);

        //to send move activity and sending data
        TomEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(getApplicationContext(),edit.class);
//                intent1.putExtra("nama", dataNama.getText());
//                intent1.putExtra("nim", dataNim.getText());
//                intent1.putExtra("alamat", dataAlamat.getText());
                startActivity(intent1);
            }
        });
        //to enable back Button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    //getting back to ListHome
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed(){

        super.onBackPressed();
    }
}

