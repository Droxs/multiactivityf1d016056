package com.myandroid.multiactivityf1d016056;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class edit extends AppCompatActivity {
    TextView eNama;
    TextView eNim;
    TextView eAlamat;
    Button tombol;
    int getPosisi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        eNama = findViewById(R.id.editNama);
        eNim = findViewById(R.id.editNim);
        eAlamat = findViewById(R.id.editAlamat);
        tombol = findViewById(R.id.TomSave);

        final Intent intent = getIntent();
        getPosisi = intent.getIntExtra("posisi",0);
        String getNama = intent.getStringExtra("nama");
        String getNim = intent.getStringExtra("nim");
        String getAlamat = intent.getStringExtra("alamat");

        eNama.setText(getNama);
        eNim.setText(getNim);
        eAlamat.setText(getAlamat);

        tombol.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(
                        getApplicationContext(),
                        "Berhasil",
                        Toast.LENGTH_LONG).show();


                Intent intent1 = new Intent(getApplicationContext(),MainActivity.class);
                intent1.putExtra("nama", ""+ eNama.getText());
                intent1.putExtra("nim",""+ eNim.getText());
                intent1.putExtra("alamat", ""+ eAlamat.getText());
                intent1.putExtra("pos",getPosisi);
                intent1.putExtra("par",1);
                startActivity(intent1);
            }
        });
    }
}

