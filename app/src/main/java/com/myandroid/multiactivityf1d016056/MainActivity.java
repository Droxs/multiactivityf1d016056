package com.myandroid.multiactivityf1d016056;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    String[] mhsNIM = {
            "F1D016056",
            "F1D016056",
            "F1D016056"
    };
    String[] mhsNama = {
            "Muhammad Ilham Darmawan",
            "Muhammad Ilham Darmawan",
            "Muhammad Ilham Darmawan"
    };
    String[] mhsAlamat = {
            "kekalik",
            "kekalik",
            "kekalik"
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listView);

        CustomAdapter customAdapter = new CustomAdapter();
        listView.setAdapter(customAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(
                        getApplicationContext(),
                        mhsNama[position],
                        Toast.LENGTH_LONG).show();
                int pos = position;
                Intent intent = new Intent(getApplicationContext(),item.class);
                intent.putExtra("posisi",pos);
                intent.putExtra("nama", mhsNama[position]);
                intent.putExtra("nim",mhsNIM[position]);
                intent.putExtra("alamat",mhsAlamat[position]);
                startActivity(intent);

            }

        });

    }

    private class CustomAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return mhsNama.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view1 = getLayoutInflater().inflate(R.layout.row, null);

            TextView mhsnim = view1.findViewById(R.id.nim);

            Intent intent2 = getIntent();
            String updateNama = intent2.getStringExtra("nama");
            String updateNim = intent2.getStringExtra("nim");
            String updateAlamat = intent2.getStringExtra ("alamat");
            int forUpdateData = intent2.getIntExtra("pos",0);
            int param = intent2.getIntExtra("par",0);

            if(param == 1){
                mhsNama[forUpdateData] = updateNama;
                mhsNIM[forUpdateData] = updateNim;
                mhsAlamat[forUpdateData] = updateAlamat;
            }

            mhsnim.setText(mhsNIM[position]);

            return view1;
        }
    }
}
